#include <stdio.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFFER_SIZE 1024

int main(int argc,char **argv)
{
	int from_fd,to_fd;
	int byte_read,byte_write;
	char buffer[BUFFER_SIZE];
	char *ptr;

	if(argc!=3)
		{
			fprintf(stderr,"Usage:%s fromfile to file/n/a",argv[0]);
			exit(1);
		}
	/* open source file */
	if((from_fd=open(argv[1],O_RDONLY))==-1)
		{
			fprintf(stderr,"Open %s Error:%s/n",argv[1],strerror(errno));
			exit(1);
		}
	/* creat file */
	if((to_fd=open(argv[2],O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR))==-1)
		{
			fprintf(stderr,"Open %s Error:%s/n",argv[2],strerror(errno));
			exit(1);
		}
	/* this is the classic code in copy file! */
	while(byte_read=read(from_fd,buffer,BUFFER_SIZE))
		{
		/* A fatal mistake has happened */
			if((byte_read==-1)&&(errno!=EINTR)) break;
			else if(byte_read>0)
				{
					ptr=buffer;
					while(byte_write=write(to_fd,ptr,byte_read))
						{/* A fatal mistake has happened */
							if((byte_write==-1)&&(errno!=EINTR)) break;
							/* All read bytes have been written */
							else if(byte_write==byte_read) break;
							else if(byte_write>0)
								{
									ptr+=byte_write;
									byte_read-=byte_write;
								}
						}
					if(byte_write==-1) break;
				}
		}
	close(from_fd);
	close(to_fd);
	exit(0);
}

