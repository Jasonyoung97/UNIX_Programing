#include <stdio.h>
#include <pthread.h>

void *myThreadl(void)
{
	int i;
	for(i=0;i<3;i++)
	{
		printf("This is the 1st pthread, creat by yang1.\n");
		sleep(1);
	}
}

void *myThread2(void)
{
	int i;
	for(i=0;i<3;i++)
	{
		printf("This is the 1st pthread, creat by yang2.\n");
		sleep(1);
	}
}

int mian(void)
{
	int i=0,ret=0;
	pthread_t id1,id2;

	ret=pthread_creat(&id1,NULL,(void*)myThreadl,NULL);
	if(ret)
	{
		printf("creat pthread error!\n");
		return 1;
	}
	
	ret=pthread_creat(&id2,NULL,(void*)myThread2,NULL);
	if(ret)
	{
		printf("creat pthread error!\n");
		return 1;
	}

	pthread_join(id1,NULL);
	pthread_join(id2,NULL);

	return 0;
}
