#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void *create(void *arg)
{
	printf("New thread......\n");
	printf("New thread id is %u \n",(unsigned int)pthread_self());
	printf("New process pid is %d\n",getpid());
	return (void *)0;
}

int mian(int argc,char *argv[])
{
	pthread_t tid;
	int error;
	printf("Main thread is starting...\n");
	
	error=pthread_create(&tid,NULL,create,NULL);
	if(error)
		{
			printf("thread is not creat!\n");
			return -1;
		}
	printf("The mian process's pid is %d \n",getpid());
	return 0;
}
