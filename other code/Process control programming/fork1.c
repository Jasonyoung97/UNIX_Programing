#include <sys/types.h>
#include <unistd.h>

main(void)
{
	pid_t pid;
	pid=fork();
	if(pid<0)
		printf("error in fork!");
	else if(pid==0)
		printf("I am the child process,ID is %d\n",getpid());
	else
		printf("I am the parent process,ID id is %d\n",getpid());
}
