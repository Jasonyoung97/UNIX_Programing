#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PERM S_IRUSR|S_IWUSR

int mian(int argc,char **argv)
{
	int shmid;
	char *p_addr,*c_addr;
	if(argc!=2)
		{
			fprintf(stderr,"Usage:%s\n\a",argv[0]);
			exit(1);
		}
	if((shmid=shmget(IPC_PRIVATE,1024,PERM))==-1)
		{
			fprintf(stderr,"Create Share Memory Error:%s\n\a",strerror(errno));
			exit(1);
		}
	if(fork())//父进程写
		{
			p_addr=shmat(shmid,0,0);//0为系统指定地址
			memset(p_addr,'\0',1024);
			strncpy(p_addr,argv[1],1024);
			wait(NULL);//释放资源
			exit(0);
		}
	else//子进程读
		{
			sleep(1);//暂停一秒,防止子进程先运行
			c_addr=shmat(shmid,0,0);
			printf("Client get %s\n",c_addr);
			exit(0);
		}
}
