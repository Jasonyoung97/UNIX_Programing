#include <stdio.h>
main()
{
	FILE *fp;
	char ch;
	if((fp=fopen("string","wt+"))==NULL)
	{
		printf("Cannot open file, Strike any key exit!");
		getch();
		exit(1);
	}
	printf("input a string:\n");
	ch=getchar();
	while(ch!='\n')
	{
		fputc(ch,fp);
		ch=getchar();
	}
	printf("\n");
	fclose(fp);
}
