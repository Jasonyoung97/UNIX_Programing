#include <stdio.h>
#include <unistd.h>

int main(void)
{
	pid_t pid;
	int count=0;
	pid=vfork();
	count ++;
	printf("count=%d\n",count);
	return 0;
}
