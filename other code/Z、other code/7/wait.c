#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

	
void main(void)
{
	pid_t pc,pr;
	pc=fork();
	if(pc==0){
		printf("This is child process with pid of %d\n",getpid());
		sleep(10);
		}
	else if(pc>0){
		pr=wait(NULL);
		printf(("I cantched a child process with pid of %d\n"),pr);
		}
	exit(0);
}

