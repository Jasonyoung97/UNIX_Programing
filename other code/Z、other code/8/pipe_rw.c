#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>

int main(){
	int pipe_fd[2];
	pid_t pid;
	char buf_r[100];
	char* p_wbuf;
	int r_num;

	memset(buf_r,0,sizeof(buf_r));
	/* creat the pipe */
	if(pipe(pipe_fd)<0)
		{
			printf("pipe creat error!\n");
			return -1;
		}
	/* creat son pid */
	if((pid=fork())==0)//son pid!?father pid?
		{
			printf("\n");
			close(pipe_fd[1]);
			sleep(2);//why sleep?
			if((r_num=read(pipe_fd[0],buf_r,100))>0)
				{
					printf("%d number read from the pipe is %s\n",r_num,buf_r);
				}
			close(pipe_fd[0]);
			exit(0);
		}
	else if(pid>0)
	{
		close(pipe_fd[0]);
		if(write(pipe_fd[1],"Hello",5)!=-1)
			printf("parent write1 Hello!\n");
		if(write(pipe_fd[1]," pipe",5)!=-1)
			printf("parent write2 pipe!\n");
		close(pipe_fd[1]);
		sleep(3);
		waitpid(pid,NULL,0);//wait for son pid over!
		exit(0);
	}
}
