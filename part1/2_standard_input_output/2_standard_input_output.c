#include "apue.h"

#define BUFFRESIZE 4096

int main(void)
{
	int n;
	char buffer[BUFFRESIZE];
	
	while((n = read(STDIN_FILENO, buffer, BUFFRESIZE)) > 0)
		if(write(STDOUT_FILENO, buffer, n) != n)
			err_sys("write error!\n");
		if(n < 0) err_sys("read error!\n");

	exit(0);
}
